# PMBFramework

[![CI Status](http://img.shields.io/travis/dennyong07@gmail.com/PMBFramework.svg?style=flat)](https://travis-ci.org/dennyong07@gmail.com/PMBFramework)
[![Version](https://img.shields.io/cocoapods/v/PMBFramework.svg?style=flat)](http://cocoapods.org/pods/PMBFramework)
[![License](https://img.shields.io/cocoapods/l/PMBFramework.svg?style=flat)](http://cocoapods.org/pods/PMBFramework)
[![Platform](https://img.shields.io/cocoapods/p/PMBFramework.svg?style=flat)](http://cocoapods.org/pods/PMBFramework)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PMBFramework is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PMBFramework'
```

## Author

dennyong07@gmail.com, Denny@pmberjaya.com

## License

PMBFramework is available under the MIT license. See the LICENSE file for more info.
