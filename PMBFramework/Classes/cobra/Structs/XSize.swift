//
//  XSize.swift
//  Cobra
//
//  Created by DickyChengg on 7/24/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import UIKit

public struct XSize {
    public static let xS: CGFloat      = 480.0
    public static let S: CGFloat       = 568.0
    public static let M: CGFloat       = 667.0
    public static let L: CGFloat       = 736.0
    public static let xL: CGFloat      = 768.0
    public static let xxL: CGFloat     = 1024.0
    public static let xxxL: CGFloat    = 1366.0
}
