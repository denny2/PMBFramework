//
//  ServerStateEn.swift
//  cobra-iOS
//
//  Created by DickyChengg on 6/20/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

public enum ServerStateEn: String {
    case alpha = "ALPHA"
    case beta = "BETA"
    case production = "PMB-release_state-PRODUCTION"
}
