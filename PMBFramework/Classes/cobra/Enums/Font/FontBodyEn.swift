//
//  FontBodyEn.swift
//  PMB iChat
//
//  Created by DickyChengg on 7/24/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import Foundation

public enum FontBodyEn {
    case b1
    case b2
    case b3
    case b4
    case b5
    case b6
    
    case f1
    case f2
    case f3
}
