//
//  SystemSoundEn.swift
//  cobra-iOS
//
//  Created by DickyChengg on 5/17/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

public enum SystemSoundEn: Int {
    case sentMail = 1001
    case receiveMail1 = 1110
    case receiveMail2 = 1022
}
