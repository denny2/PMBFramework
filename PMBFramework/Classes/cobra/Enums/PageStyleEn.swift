//
//  PageStyleEn.swift
//  SGByte-Property
//
//  Created by Roni Aja on 8/8/17.
//  Copyright © 2017 Pundi Mas Berjaya. All rights reserved.
//

import Foundation

public enum PageStyleEn {
    case segment
    case scroll
}
