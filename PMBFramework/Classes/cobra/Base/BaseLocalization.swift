//
//  BaseLocalization.swift
//  Alamofire
//
//  Created by Denny Ong on 10/4/18.
//

import Foundation
import Localize_Swift

open class BaseLocalization {
    public var bundle = "Base"
    
    public init(_ withBundle: String) {
        self.bundle = withBundle
    }
    
    public func set(_ text: String) -> String {
        return text.localized(using: bundle)
    }
    
    class func set(_ text: String) -> String {
        return text.localized(using: "Base")
    }
}
