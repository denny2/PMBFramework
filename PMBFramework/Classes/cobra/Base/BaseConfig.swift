//
//  BaseConfig.swift
//  SGBytePhone
//
//  Created by DickyChengg on 12/7/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import Foundation

open class BaseConfig {
    
    open class func initialize() {
        DSource.enableConstraintLogging(false)
        DSource.enableParametersLogging(true)
        DSource.enableRestApiLogging(true)
        DSource.enableDebugLogging(true)
        DSource.setupFontSize()
    }
}
