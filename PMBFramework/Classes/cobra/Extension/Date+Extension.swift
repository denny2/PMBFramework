//
//  Date+Extension.swift
//  PMB iChat
//
//  Created by DickyChengg on 8/11/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import UIKit
import SwiftDate

extension Date {
    
    public static func now() -> DateInRegion {
//        return Date().add(components: [Calendar.Component.hour : 7])
        return Date().dateByAdding(7, Calendar.Component.hour)
    }

    public func now() -> DateInRegion {
//        return self.add(components: [Calendar.Component.hour : 7])
        return self.dateByAdding(7, Calendar.Component.hour)
    }

    public static func currentTimeInterval() -> TimeInterval {
//        return Date.now().timeIntervalSince1970
        return Date.timeIntervalBetween1970AndReferenceDate
    }
    
    public var minGMT7: DateInRegion {
//        return self.add(components: [Calendar.Component.hour: -7])
        return self.dateByAdding(7, Calendar.Component.hour)
    }
    
}
