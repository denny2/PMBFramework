//
//  Array+Extension.swift
//  PMB iChat
//
//  Created by DickyChengg on 9/5/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import Foundation

extension Array {
    
    public mutating func appendFirst(_ value: Element) {
        insert(value, at: 0)
    }
    
    public mutating func append(_ values: [Element]) {
        self += values
    }
    
    public mutating func appendFirst(_ values: [Element]) {
        self = values + self
    }
    
    public func get(_ index: Int) -> Element? {
        guard index >= 0 && index < self.count else {
            return nil
        }
        return self[index]
    }
    
    public func get(_ index: Int, _ null: Element) -> Element {
        guard index >= 0 && index < self.count else {
            return null
        }
        return self[index]
    }
}

extension Array where Element: UIView {
    
    func setup(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
    
}

extension Array where Element: UIBarItem {
    
    func setup(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
    
}

extension Array where Element: UIGestureRecognizer {
    
    func setup(_ callback: ((Element)->Void)) {
        for item in self {
            callback(item)
        }
    }
    
}
