//
//  UIColor+Extension.swift
//  cobra-iOS
//
//  Created by DickyChengg on 2/20/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init(hexa: Int) {
        let mask = 0xFF
        let limit: CGFloat = 255.0
        let red = CGFloat((hexa >> 16) & mask) / limit
        let green = CGFloat((hexa >> 8) & mask) / limit
        let blue = CGFloat(hexa & mask) / limit
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
    
    public func alpha(_ value: CGFloat) -> UIColor {
        return self.withAlphaComponent(value)
    }
    
    open class var primary: UIColor {
        return BaseColor.primary
    }
    
    open class var secondary: UIColor {
        return BaseColor.secondary
    }
    
    open class var background: UIColor {
        return UIColor.white
    }
    
    open class var backgroundAlpha: UIColor {
        // like fader but used for background
        return UIColor.grayAlpha.alpha(0.4)
    }
    
    open class var firstGray: UIColor {
        // first dark gray
        return UIColor.init(hexa: 0x353535)
    }
    
    open class var secondGray: UIColor {
        // second dark gray
        return UIColor.init(hexa: 0x555555)
    }
    
    open class var thirdGray: UIColor {
        // first light gray
        return UIColor.init(hexa: 0x757575)
    }
    
    open class var lastGray: UIColor {
        // second light gray
        return UIColor.init(hexa: 0xA5A5A5)
    }
    
    open class var silverGray: UIColor {
        // pure silver color
        return UIColor.init(hexa: 0xF0F0F0)
    }
    
    open class var grayAlpha: UIColor {
        // like fader
        return UIColor.init(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5)
    }
    
    open class var blackAlpha: UIColor {
        return UIColor.init(hexa: 0xB20000)
    }
    
    public class Main {
        
        open class var red: UIColor {
            return UIColor.init(hexa: 0xFF3B30)
        }
        
        open class var orange: UIColor {
            return UIColor.init(hexa: 0xFF9500)
        }
        
        open class var yellow: UIColor {
            return UIColor.init(hexa: 0xFFCC00)
        }
        
        open class var green: UIColor {
            return UIColor.init(hexa: 0x4CD964)
        }
        
        open class var tealBlue: UIColor {
            return UIColor.init(hexa: 0x5AC8FA)
        }
        
        open class var blue: UIColor {
            return UIColor.init(hexa: 0x007AFF)
        }
        
        open class var purple: UIColor {
            return UIColor.init(hexa: 0x5854D6)
        }
        
        open class var pink: UIColor {
            return UIColor.init(hexa: 0xFF2D55)
        }
    }
    
    public class Bootstrap {
        
        // red
        open class var redSolid: UIColor {
            return UIColor(hexa: 0xD9534F)
        }
        
        open class var redLight: UIColor {
            return UIColor(hexa: 0xF2DEDE)
        }
        
        open class var redSolidBorder: UIColor {
            return UIColor(hexa: 0xD43F3A)
        }
        
        open class var redLightBorder: UIColor {
            return UIColor(hexa: 0xEBCCD1)
        }
        
        open class var redText: UIColor {
            return UIColor(hexa: 0xA94442)
        }
        
        
        
        // yellow
        open class var yellowSolid: UIColor {
            return UIColor(hexa: 0xEC971F)
        }
        
        open class var yellowLight: UIColor {
            return UIColor(hexa: 0xFCF8E3)
        }
        
        open class var yellowSolidBorder: UIColor {
            return UIColor(hexa: 0xD58512)
        }
        
        open class var yellowLightBorder: UIColor {
            return UIColor(hexa: 0xFAEBCC)
        }
        
        open class var yellowText: UIColor {
            return UIColor(hexa: 0x8A6D3B)
        }
        
        
        
        // green
        open class var greenSolid: UIColor {
            return UIColor(hexa: 0x5CB85C)
        }
        
        open class var greenLight: UIColor {
            return UIColor(hexa: 0xDff0D8)
        }
        
        open class var greenSolidBorder: UIColor {
            return UIColor(hexa: 0x4CAE4C)
        }
        
        open class var greenLightBorder: UIColor {
            return UIColor(hexa: 0xD6E9C6)
        }
        
        open class var greenText: UIColor {
            return UIColor(hexa: 0x3C763D)
        }
        
        
        
        // blue
        open class var blueSolid: UIColor {
            return UIColor(hexa: 0x5BC0DA)
        }
        
        open class var blueLight: UIColor {
            return UIColor(hexa: 0xD9EDF7)
        }
        
        open class var blueSolidBorder: UIColor {
            return UIColor(hexa: 0x46B8DA)
        }
        
        open class var blueLightBorder: UIColor {
            return UIColor(hexa: 0xBCE8F1)
        }
        
        open class var blueText: UIColor {
            return UIColor(hexa: 0x31708F)
        }
        
        
        
        // primary
        open class var primarySolid: UIColor {
            return UIColor(hexa: 0x286090)
        }
        
        open class var primarySolidBorder: UIColor {
            return UIColor(hexa: 0x204D74)
        }
        
    }
    
}
