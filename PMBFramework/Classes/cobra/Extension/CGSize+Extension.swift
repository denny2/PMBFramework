//
//  CGSize+Extension.swift
//  cobra-iOS
//
//  Created by DickyChengg on 3/3/17.
//  Copyright © 2017 DickyChengg. All rights reserved.
//

import UIKit

extension CGSize {
    
    public init(_ size: CGFloat) {
        width = size
        height = size
    }
    
}
