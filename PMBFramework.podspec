#
# Be sure to run `pod lib lint PMBFramework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PMBFramework'
  s.version          = '2.1.4'
  s.summary          = 'PMBFramework is a private lib for PMB IOS Developer'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
PMBFramework lib for PMB ios developer.
                       DESC

  s.homepage         = 'https://gitlab.com/denny2/PMBFramework.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Denny' => 'dennyong07@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/denny2/PMBFramework.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'PMBFramework/Classes/**/*'
  
  # s.resource_bundles = {
  #   'PMBFramework' => ['PMBFramework/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
    s.frameworks = 'UIKit'
    s.dependency 'Alamofire'
    s.dependency 'Localize-Swift'
    s.dependency 'RxSwift'
    s.dependency 'SnapKit'
    s.dependency 'SwiftDate'
    s.dependency 'SwiftyJSON'
    s.dependency 'Kingfisher', '4.9.0'
    s.dependency 'RappleProgressHUD'
    s.dependency 'RxDataSources'
end
